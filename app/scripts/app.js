'use strict';

/**
 * @ngdoc overview
 * @name vyjebFrontendApp
 * @description
 * # vyjebFrontendApp
 *
 * Main module of the application.
 */
angular
    .module('angularCoreApp', [
        'ngAnimate',
        'ngCookies',
        'ngResource',
        'ngRoute',
        'ngSanitize',
        'pascalprecht.translate',
        'angular-loading-bar',
        'ngMaterial'
    ])
    .config(function ($routeProvider) {
        $routeProvider
            .when('/', {
                templateUrl: 'views/home.html',
                controller: 'HomeCtrl',
                controllerAs: 'home'
            })
            .when('/contact', {
                templateUrl: 'views/contact.html',
                controller: 'ContactCtrl',
                controllerAs: 'contact'
            })
            .when('/kontakt', {
                templateUrl: 'views/contact.html',
                controller: 'ContactCtrl',
                controllerAs: 'contact'
            })
            .otherwise({
                redirectTo: '/'
            });
    })
    .config(function ($translateProvider) {
        $translateProvider.useSanitizeValueStrategy();
        $translateProvider.useStaticFilesLoader({
          prefix: 'locales/',
          suffix: '.json'
        });
        $translateProvider.preferredLanguage('en');
        //$translateProvider.preferredLanguage(window.navigator.language.slice(0, 2));
    })
    .config(function($mdThemingProvider) {
        $mdThemingProvider.theme('default');
    })
    .run(function($rootScope, $location, $translate) {
        var translate = function () {
            var location = $location.path();
            if (location === "/") {
                $translate('MENU.HOME').then(function (translation) {
                    document.title = translation;
                    $rootScope.documentTitle = translation;
                });
            } else {
                location = location.toUpperCase();
                location = location.split("/");

                        $translate('ROUTE.' + location[1]).then(function (translation) {
                            document.title = translation;
                            $rootScope.documentTitle = translation;
                        });

            }
        };

        $rootScope.$on('$routeChangeSuccess', function() {
            translate();
        });

        $rootScope.$on('$translateChangeSuccess', function () {
            translate();
        });
    })
    .controller('RouteCtrl', function ($scope) {
    })
    .controller('LocaleCtrl', function ($scope, $translate, $location, $rootScope) {

        $scope.changeLocale = function (locale) {
            var location = $location.path();

            if (location === "/") {
                $translate('MENU.HOME').then(function (translation) {
                    document.title = translation;
                    $rootScope.documentTitle = translation;
                    $rootScope.locale = locale;
                    $translate.use(locale);
                });
            } else {
                location = location.toUpperCase();
                location = location.split("/");
                $translate('REDIRECT.' + location[1]).then(function (translation) {
                    translation = translation.toLowerCase();
                    $location.url(translation);
                    $rootScope.locale = locale;
                    $translate.use(locale);
                });
            }
        };
    })
    .controller('AppCtrl', function ($scope, $timeout, $mdSidenav, $translate, $location, $rootScope, $mdToast) {

        $scope.showTranslationToast = function() {
            $translate('TRANSLATE_MESSAGE').then(function (translation) {
                var toast = $mdToast.simple()
                    .textContent(translation)
                    .position('top right')
                    .hideDelay(2000);

                $mdToast.show(toast);
            });
        };






        var location = $location.path();



        if (location === "/") {
            $rootScope.locale = "en";
        } else {
            location = location.toUpperCase();
            location = location.split("/");

            $translate('LANGUAGE_OF_ROUTE.' + location[1]).then(function (LANGUAGE_OF_ROUTE) {
                $translate.use(LANGUAGE_OF_ROUTE).then(function () {
                    $translate('ROUTE.' + location[1]).then(function (translation) {
                        document.title = translation;
                        $rootScope.documentTitle = translation;
                        $rootScope.locale = LANGUAGE_OF_ROUTE;

                        if (window.navigator.language.slice(0, 2) !== $rootScope.locale) {
                            $scope.showTranslationToast();
                        }
                    });
                });
            });
        }



        function debounce(func, wait) {
            var timer;
            return function debounced() {
                var context = $scope,
                    args = Array.prototype.slice.call(arguments);
                $timeout.cancel(timer);
                timer = $timeout(function() {
                    timer = undefined;
                    func.apply(context, args);
                }, wait || 10);
            };
        }

        /**
         * Build handler to open/close a SideNav; when animation finishes
         * report completion in console
         */

        function buildDelayedToggler(navID) {
            return debounce(function() {
                // Component lookup should always be available since we are not using `ng-if`
                $mdSidenav(navID)
                    .toggle()
                    .then(function () {
                        //$log.debug("toggle " + navID + " is done");
                    });
            }, 200);
        }

        function buildToggler(navID) {
            return function() {
                // Component lookup should always be available since we are not using `ng-if`
                $mdSidenav(navID)
                    .toggle()
                    .then(function () {
                        //$log.debug("toggle " + navID + " is done");
                    });
            };
        }

        $scope.toggleLeft = buildDelayedToggler('left');
        $scope.toggleRight = buildToggler('right');

        $scope.isOpenRight = function(){
            return $mdSidenav('right').isOpen();
        };
    })
    .controller('LeftCtrl', function ($scope, $timeout, $mdSidenav, $location, $route, $translate) {
        $scope.close = function () {
            // Component lookup should always be available since we are not using `ng-if`
            $mdSidenav('left').close();
        };
        $scope.href = function (location) {
            if (location === "/") {
                $location.url("/");
            } else {
                location = location.toUpperCase();
                $translate('MENU.' + location).then(function (translation) {
                    translation = translation.toLowerCase();
                    $location.url(translation);
                });
            }
        };
        $scope.tab = function(route) {
            return $route.current && route === $route.current.controller;
        };
    });

